import { validateTimeRecord } from '../validator';
import twoWayCoverageData from '../__testsData__/twoWayCoverageData';
import threeWayCoverageData from '../__testsData__/threeWayCoverageData';

describe('validateInsurance', () => {
  it.each(twoWayCoverageData)(
    'test validateTimeRecord with 2-way coverage (%s, %s, %s, ...)',
    (isSuccessExpected, startDateTime, endDateTime, note, project) => {
      const init = {
        elements: {
          endDateTime: null,
          note: null,
          project: { id: null },
          startDateTime: null,
        },
        isValid: true,
      };
      const result = validateTimeRecord({
        endDateTime,
        note,
        project: { id: project },
        startDateTime,
      }, init);

      expect(result.isValid).toEqual(isSuccessExpected);
    },
  );

  it.each(threeWayCoverageData)(
    'test validateTimeRecord with 3-way coverage (%s, %s, %s, ...)',
    (isSuccessExpected, startDateTime, endDateTime, note, project) => {
      const init = {
        elements: {
          endDateTime: null,
          note: null,
          project: { id: null },
          startDateTime: null,
        },
        isValid: true,
      };
      const result = validateTimeRecord({
        endDateTime,
        note,
        project: { id: project },
        startDateTime,
      }, init);

      expect(result.isValid).toEqual(isSuccessExpected);
    },
  );
});
