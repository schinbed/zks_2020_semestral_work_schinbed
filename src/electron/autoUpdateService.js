/* eslint-disable */
import {
  dialog,
  Notification,
} from 'electron';
import { autoUpdater } from 'electron-updater';

export const init = () => {
  autoUpdater.checkForUpdates();

  autoUpdater.on('update-available', (info) => {
    if (!Notification.isSupported()) {
      return;
    }

    new Notification({
      body: `Nová verze ${info.version} je k dispozici. Probíhá stahování.`,
      title: 'eOSVČ',
    }).show();
  });

  autoUpdater.on('error', () => {
    if (!Notification.isSupported()) {
      return;
    }

    new Notification({
      body: 'Nastala chyba při stahování nové verze aplikace.',
      title: 'eOSVČ',
    }).show();
  });

  autoUpdater.on('update-downloaded', (info) => {
    const clickedButtonIndex = dialog.showMessageBoxSync({
      buttons: ['Ano', 'Ne'],
      message: `Nová verze ${info.version} je k dispozici. Chcete ji nainstalovat?`,
      title: 'Nová verze aplikace',
      type: 'info',
    });

    if (clickedButtonIndex === 0) {
      autoUpdater.quitAndInstall(true, true);
    }
  });
};
