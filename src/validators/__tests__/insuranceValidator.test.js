import { validateInsurance } from '../insuranceValidator';
import twoWayCoverageData from '../__testsData__/twoWayCoverageData';
import threeWayCoverageData from '../__testsData__/threeWayCoverageData';

describe('validateInsurance', () => {
  it.each(twoWayCoverageData)(
    'test validateInsurance with 2-way coverage (%s, %s, %s, %s, ...)',
    (isSuccessExpected, date, amount, variant, note) => {
      const init = {
        elements: {
          amount: null,
          date: null,
          note: null,
          variant: null,
        },
        isValid: true,
      };
      const result = validateInsurance({
        amount,
        date,
        note,
        variant,
      }, init);

      expect(result.isValid).toEqual(isSuccessExpected);
    },
  );

  it.each(threeWayCoverageData)(
    'test validateInsurance with 3-way coverage (%s, %s, %s, %s, ...)',
    (isSuccessExpected, date, amount, variant, note) => {
      const init = {
        elements: {
          amount: null,
          date: null,
          note: null,
          variant: null,
        },
        isValid: true,
      };
      const result = validateInsurance({
        amount,
        date,
        note,
        variant,
      }, init);

      expect(result.isValid).toEqual(isSuccessExpected);
    },
  );
});
