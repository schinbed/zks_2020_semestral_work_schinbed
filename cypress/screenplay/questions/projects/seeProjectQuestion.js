import { createQuestion } from 'cypress-screenplay';

export const seeProjectQuestion = createQuestion((cy, project) => {
  cy.get('td').should('exist').contains(project);
});
