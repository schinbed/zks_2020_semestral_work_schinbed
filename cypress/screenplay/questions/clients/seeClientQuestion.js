import { createQuestion } from 'cypress-screenplay';

export const seeClientQuestion = createQuestion((cy, client) => {
  cy.get('td').should('exist').contains(client);
});
