import { createQuestion } from 'cypress-screenplay';

export const seeFormFieldErrorQuestion = createQuestion((cy, { message, repeat }) => {
  cy.get('.MuiFormHelperText-root')
    .should('exist')
    .should('have.length', repeat)
    .contains(message);
});
