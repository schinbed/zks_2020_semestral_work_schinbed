import { createQuestion } from 'cypress-screenplay';

export const seeMessageQuestion = createQuestion((cy, message) => {
  cy.get('.MuiAlert-message').should('exist').contains(message);
});
