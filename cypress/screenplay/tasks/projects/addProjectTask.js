import { createTask } from 'cypress-screenplay';
import {
  clickButton,
  fillField,
} from '../../../support/helpers';

export const addProjectTask = createTask((cy, data) => {
  const {
    projectData,
    userData,
  } = data;

  cy.intercept(
    'POST',
    '/projects',
      {
        body: {
          id: projectData.id,
          name: projectData.name,
          client: {
            id: projectData.client.id,
            name: projectData.client.name,
          },
          owner: {
            id: userData.id,
          },
        },
        statusCode: 201,
      },
  ).as('postProject');

  cy.wait(500);
  clickButton(cy, '#addProjectButton');
  cy.wait(500);

  fillField(cy, projectData.name, '#name');
  clickButton(cy, '#saveProjectButton');

  cy.wait('@postProject');
  cy.wait('@getProjects');
});
