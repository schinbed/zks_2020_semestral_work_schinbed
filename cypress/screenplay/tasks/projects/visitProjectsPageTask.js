import { createTask } from 'cypress-screenplay';

export const visitProjectsPageTask = createTask((cy, data) => {
  const {
    clientsData,
    projectData,
    userData,
  } = data;

  let interceptGetProjectsCount = 0;

  cy.intercept(
    'GET',
    '/users/1/clients',
    clientsData,
  ).as('getClients');
  cy.intercept(
  'GET',
  '/users/1/projects',
  (req) => {
    if (interceptGetProjectsCount % 2 === 0 ) {
      req.reply({
        body: [],
        statusCode: 200,
      });
    } else {
      req.reply({
        body: [{
          id: projectData.id,
          name: projectData.name,
          client: {
            id: projectData.client.id,
            name: projectData.client.name,
          }
        }],
        statusCode: 200,
      });
    }

    interceptGetProjectsCount += 1;
  },
  ).as('getProjects');
  cy.intercept(
  'GET',
  '/users/1',
    userData,
  ).as('getUser');

  cy.visit('/projekty', {
    onBeforeLoad (win) {
      delete win.navigator.__proto__.serviceWorker;
    }
  });

  cy.wait('@getUser');
  cy.wait('@getClients');
  cy.wait('@getProjects');
});
