import { createTask } from 'cypress-screenplay';
import { clickButton } from '../../../support/helpers';

export const tryToAddProjectWithEmptyDataTask = createTask((cy) => {
  cy.wait(500);
  clickButton(cy, '#addProjectButton');
  cy.wait(500);

  clickButton(cy, '#saveProjectButton');
});
