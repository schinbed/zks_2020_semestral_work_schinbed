import { createTask } from 'cypress-screenplay';

export const visitClientsPageTask = createTask((cy, data) => {
  const {
    clientData,
    userData,
  } = data;

  let interceptGetClientsCount = 0;

  cy.intercept(
  'GET',
  '/users/1/clients',
  (req) => {
    if (interceptGetClientsCount % 2 === 0 ) {
      req.reply({
        body: [],
        statusCode: 200,
      });
    } else {
      req.reply({
        body: [{
          id: clientData.id,
          name: clientData.name,
          street: clientData.street,
          city: clientData.city,
          postalCode: clientData.postalCode,
          cidNumber: null,
          taxNumber: null,
          contactEmail: null,
          contactPhoneNumber: null,
          owner: {
            id: userData,
          },
        }],
        statusCode: 200,
      });
    }

    interceptGetClientsCount += 1;
  },
  ).as('getClients');
  cy.intercept(
  'GET',
  '/users/1',
    userData,
  ).as('getUser');

  cy.visit('/klienti', {
    onBeforeLoad (win) {
      delete win.navigator.__proto__.serviceWorker;
    }
  });

  cy.wait('@getUser');
  cy.wait('@getClients');
});
