import { createTask } from 'cypress-screenplay';
import {
  clickButton,
  fillField,
} from '../../../support/helpers';

export const addClientTask = createTask((cy, data) => {
  const {
    clientData,
    userData,
  } = data;

  cy.intercept(
    'POST',
    '/clients',
      {
        body: {
          id: clientData.id,
          name: clientData.name,
          street: clientData.street,
          city: clientData.city,
          postalCode: clientData.postalCode,
          cidNumber: null,
          taxNumber: null,
          contactEmail: null,
          contactPhoneNumber: null,
          owner: {
            id: userData.id,
          },
        },
        statusCode: 201,
      },
  ).as('postClient');

  cy.wait(500);
  clickButton(cy, '#addClientButton');
  cy.wait(500);

  fillField(cy, clientData.name, '#name');
  fillField(cy, clientData.street, '#street');
  fillField(cy, clientData.city, '#city');
  fillField(cy, clientData.postalCode, '#postalCode');
  clickButton(cy, '#saveClientButton');

  cy.wait('@postClient');
  cy.wait('@getClients');
});
