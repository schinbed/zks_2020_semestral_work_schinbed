import { createTask } from 'cypress-screenplay';
import { clickButton } from '../../../support/helpers';

export const tryToAddClientWithEmptyDataTask = createTask((cy) => {
  cy.wait(500);
  clickButton(cy, '#addClientButton');
  cy.wait(500);

  clickButton(cy, '#saveClientButton');
});
