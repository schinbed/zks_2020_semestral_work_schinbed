import login200Data from '../fixtures/login.200.json';
import login401Data from '../fixtures/login.401.json';
import DashboardPage from '../pageObjects/DashboardPage';
import LoginPage from '../pageObjects/LoginPage';

describe('Log in', () => {
  beforeEach(() => {
    cy.clearLocalStorage();
  });

  it('should log in successfully', () => {
    cy.intercept(
      'POST',
      '/auth',
      login200Data,
    ).as('logIn');

    const dashboardPage = new DashboardPage();
    const loginPage = new LoginPage();

    loginPage.visit();

    loginPage.fillUsername('username');
    loginPage.fillPassword('password');
    loginPage.submit();

    cy.wait('@logIn');

    dashboardPage
      .getTitle()
      .should('exist');
  });

  it('should fail with wrong credentials', () => {
    cy.intercept(
      'POST',
      '/auth',
      login401Data,
    ).as('logIn');

    const loginPage = new LoginPage();

    loginPage.visit();

    loginPage.fillUsername('username');
    loginPage.fillPassword('password');
    loginPage.submit();

    cy.wait('@logIn');

    loginPage
      .getMessage()
      .should('exist')
      .contains('Uživatelské jméno nebo heslo je špatně.');
  });
});
