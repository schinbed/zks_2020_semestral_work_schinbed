import { Actor } from 'cypress-screenplay';
import { seeClientQuestion } from '../screenplay/questions/clients/seeClientQuestion';
import { seeFormFieldErrorQuestion } from '../screenplay/questions/clients/seeFormFieldErrorQuestion';
import { seeMessageQuestion } from '../screenplay/questions/clients/seeMessageQuestion';
import { addClientTask } from '../screenplay/tasks/clients/addClientTask';
import { tryToAddClientWithEmptyDataTask } from '../screenplay/tasks/clients/tryToAddClientWithEmptyDataTask';
import { visitClientsPageTask } from '../screenplay/tasks/clients/visitClientsPageTask';
import user200Data from '../fixtures/user.200.json';

describe('Add client', () => {
  beforeEach(() => {
    cy.login();
  });

  afterEach(() => {
    cy.clearLocalStorage();
  });

  it('should add client', () => {
    const data = {
      clientData: {
        id: 1,
        name: 'Klient',
        street: 'Ulice 123',
        city: 'Město',
        postalCode: 12345,
      },
      userData: user200Data.body,
    };

    const actor = new Actor();
    actor
      .perform(visitClientsPageTask, data)
      .perform(addClientTask, data)
      .ask(seeMessageQuestion, 'Klient byl úspěšně přidán', null)
      .ask(seeClientQuestion, data.clientData.name, null);
  });

  it('should fail when fields are not filled', () => {
    const data = { userData: user200Data.body };

    const actor = new Actor();
    actor
      .perform(visitClientsPageTask, data)
      .perform(tryToAddClientWithEmptyDataTask, null)
      .ask(seeFormFieldErrorQuestion, { message: 'Toto pole je povinné', repeat: 4 }, null);
  });
});
