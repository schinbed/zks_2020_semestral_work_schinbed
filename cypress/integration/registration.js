import registration201Data from '../fixtures/registration.201.json';
import registration400Data from '../fixtures/registration.400.json';
import RegistrationPage from '../pageObjects/RegistrationPage';

describe('Registration', () => {
  const fillFirstStepForm = (registrationPage) => {
    registrationPage.fillFirstName('Jan');
    registrationPage.fillLastName('Novak');
    registrationPage.fillEmail('jan.novak@example.com');
    registrationPage.fillUsername('jan.novak1');
    registrationPage.fillPassword('password');
  };

  const fillSecondStepForm = (registrationPage) => {
    registrationPage.fillStreet('Ulice 123');
    registrationPage.fillCity('Město');
    registrationPage.fillPostalCode(12345);
    registrationPage.fillCidNumber(12345678);
    registrationPage.fillBankAccount('12-3456789/0000')
  };

  const processForm = (registrationPage) => {
    fillFirstStepForm(registrationPage);
    registrationPage.continue();

    fillSecondStepForm(registrationPage);
    registrationPage.register();
  };

  it('should register successfully', () => {
    cy.intercept(
      'POST',
      '/users',
      registration201Data,
    ).as('registration');

    const registrationPage = new RegistrationPage();

    registrationPage.visit();
    processForm(registrationPage);

    cy.wait('@registration');

    registrationPage
      .getMessage()
      .should('exist')
      .contains('Váš účet byl vytvořen.');
  });

  it('should fail when fields are not filled', () => {
    const registrationPage = new RegistrationPage();

    registrationPage.visit();
    registrationPage.continue();

    registrationPage
      .getFormFieldHelper()
      .should('exist')
      .should('have.length', 5)
      .contains('Toto pole je povinné.');
  });

  it('should fail when API responses with error that user already exists', () => {
    cy.intercept(
      'POST',
      '/users',
      registration400Data,
    ).as('registration');

    const registrationPage = new RegistrationPage();

    registrationPage.visit();
    processForm(registrationPage);

    cy.wait('@registration');

    registrationPage
      .getMessage()
      .should('exist')
      .contains('Vytvoření účtu se nezdařilo.');
  });
});
