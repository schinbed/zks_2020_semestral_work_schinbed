import { Actor } from 'cypress-screenplay';
import { seeFormFieldErrorQuestion } from '../screenplay/questions/projects/seeFormFieldErrorQuestion';
import { seeMessageQuestion } from '../screenplay/questions/projects/seeMessageQuestion';
import { addProjectTask } from '../screenplay/tasks/projects/addProjectTask';
import { tryToAddProjectWithEmptyDataTask } from '../screenplay/tasks/projects/tryToAddProjectWithEmptyDataTask';
import { visitProjectsPageTask } from '../screenplay/tasks/projects/visitProjectsPageTask';
import clients200Data from '../fixtures/clients.200.json';
import user200Data from '../fixtures/user.200.json';
import {seeProjectQuestion} from "../screenplay/questions/projects/seeProjectQuestion";

describe('Add project', () => {
  beforeEach(() => {
    cy.login();
  });

  afterEach(() => {
    cy.clearLocalStorage();
  });

  it('should add client', () => {
    const data = {
      clientsData: clients200Data.body,
      projectData: {
        id: 1,
        name: 'Project',
        client: {
          id: 1,
          name: 'Klient',
        },
      },
      userData: user200Data.body,
    };

    const actor = new Actor();
    actor
      .perform(visitProjectsPageTask, data)
      .perform(addProjectTask, data)
      .ask(seeMessageQuestion, 'Projekt byl úspěšně přidán', null)
      .ask(seeProjectQuestion, data.projectData.name, null);
  });

  it('should fail when fields are not filled', () => {
    const data = {
      clientsData: clients200Data.body,
      userData: user200Data.body,
    };

    const actor = new Actor();
    actor
      .perform(visitProjectsPageTask, data)
      .perform(tryToAddProjectWithEmptyDataTask, null)
      .ask(seeFormFieldErrorQuestion, { message: 'Toto pole je povinné', repeat: 1 }, null);
  });
});
