import { storeTokenToStorage} from '../../src/resources/auth/storage';

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('login', () => {
  storeTokenToStorage('eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxIiwiSXNzdWVyIjoiQ3lwcmVzcyIsImV4cCI6MTg5MzQ1NjAwMCwiaWF0IjoxNjEwMjI0NjczfQ.l_zBHDRC9VpJPqgUkWVG7lRym6PY3n_l-M-iTe3Qo54');
});
