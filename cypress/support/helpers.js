export const clickButton = (cy, selector) => {
  const button = cy.get(selector);
  button.click();

  return button;
};

export const fillField = (cy, value, selector) => {
  const field = cy.get(selector);
  field.clear();
  field.type(value);

  return field;
};

export const selectItem = (cy, value, selector) => {
  const field = cy.get(selector);
  field.select(value);

  return field;
};
