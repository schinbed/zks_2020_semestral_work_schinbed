import {
  clickButton,
  fillField,
} from '../support/helpers';

class LoginPage {
  visit() {
    cy.visit('/prihlaseni');
  }

  getMessage() {
    return cy.get('.MuiAlert-message');
  }

  fillUsername(value) {
    fillField(cy, value, '#username');

    return this;
  }

  fillPassword(value) {
    fillField(cy, value, '#password');

    return this;
  }

  submit() {
    clickButton(cy, '#logIn');

    return this;
  }
}

export default LoginPage;
