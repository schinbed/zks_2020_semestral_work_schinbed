import {
  clickButton,
  fillField,
} from '../support/helpers';

class RegistrationPage {
  visit() {
    cy.visit('/vytvoreni-uctu');
  }

  getMessage() {
    return cy.get('.MuiAlert-message');
  }

  getFormFieldHelper() {
    return cy.get('.MuiFormHelperText-root');
  }

  fillFirstName(value) {
    fillField(cy, value, '#firstName');

    return this;
  }

  fillLastName(value) {
    fillField(cy, value, '#lastName');

    return this;
  }

  fillEmail(value) {
    fillField(cy, value, '#email');

    return this;
  }

  fillUsername(value) {
    fillField(cy, value, '#username');

    return this;
  }

  fillPassword(value) {
    fillField(cy, value, '#plainPassword');

    return this;
  }

  fillStreet(value) {
    fillField(cy, value, '#street');

    return this;
  }

  fillCity(value) {
    fillField(cy, value, '#city');

    return this;
  }

  fillPostalCode(value) {
    fillField(cy, value, '#postalCode');

    return this;
  }

  fillCidNumber(value) {
    fillField(cy, value, '#cidNumber');

    return this;
  }

  fillTaxCode(value) {
    fillField(cy, value, '#taxNumber');

    return this;
  }

  fillBankAccount(value) {
    fillField(cy, value, '#bankAccount');

    return this;
  }

  continue() {
    clickButton(cy, '#continueButton');

    return this;
  }

  register() {
    clickButton(cy, '#registerButton');

    return this;
  }
}

export default RegistrationPage;
