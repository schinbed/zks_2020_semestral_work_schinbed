class DashboardPage {
  visit() {
    cy.visit('/');
  }

  getTitle() {
    return cy.get('h1');
  }
}

export default DashboardPage;
